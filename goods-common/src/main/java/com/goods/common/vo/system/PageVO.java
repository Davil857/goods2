package com.goods.common.vo.system;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;


@Data
public class PageVO<T> {
    private long total;

    private List<T> rows=new ArrayList<>();
    //之所以要封装vo 是因为前端要的是rows，不能直接将List传过去，要赋值给rows
    public PageVO(long total, List<T> data) {
        this.total = total;
        this.rows = data;
    }
}
