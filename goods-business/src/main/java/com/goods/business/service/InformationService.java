package com.goods.business.service;

import com.goods.common.model.business.Product;
import com.goods.common.vo.business.ProductStockVO;
import com.goods.common.vo.business.ProductVO;
import com.goods.common.vo.system.PageVO;

import java.util.Map;

public interface InformationService {
    /**
     * http://www.localhost:8989/business/product/findProductList?pageNum=1&pageSize=6&name=&categoryId=&supplier=&status=0
     * 分页列表展示
     */
    PageVO<ProductVO> findProductList(Integer pageNum, Integer pageSize, Map map);
    /**
     * http://www.localhost:8989/business/product/add
     * 添加
     */
    void addProductList(Product product);


    /**
     * http://www.localhost:8989/business/product/edit/61
     * 回显修改
     */
    Product editProductList(Long id);
    /**
     * http://www.localhost:8989/business/product/update/61
     * 保存修改
     */
    void updateProductList(Long id, Product product);
    /**
     * http://www.localhost:8989/business/product/remove/66
     * 移到回收站
     */
    void removeProductList(Long id);
    /**
     * http://www.localhost:8989/business/product/back/66
     * 回收站恢复
     */
    void backProductList(Long id);
    /**
     * http://www.localhost:8989/business/product/delete/68
     * 回收站删除
     */
    void deleteProductList(Long id);
    /**
     * http://www.localhost:8989/business/product/publish/62
     * 通过审核
     */
    void publishProductList(Long id);
    /**
     * http://www.localhost:8989/business/product/findProductStocks?pageSize=9&pageNum=1&categorys=81,98,99
     * 库存图右侧分页查询
     */
    PageVO<ProductStockVO> findProductStocks(Integer pageNum, Integer pageSize, Map map);
}
