package com.goods.business.service;

import com.goods.common.model.business.ProductCategory;
import com.goods.common.vo.business.ProductCategoryTreeNodeVO;
import com.goods.common.vo.system.PageVO;

import java.util.List;

public interface ClassService {

    PageVO<ProductCategoryTreeNodeVO> getcategoryTree(Integer pageNum, Integer pageSize);

    /**
     * 加载父类数据用于添加
     * http://www.localhost:8989/business/productCategory/getParentCategoryTree
     */
    List<ProductCategoryTreeNodeVO> getParentCategoryTree();

    /**
     * 添加
     * http://www.localhost:8989/business/productCategory/add
     */
    void addParentCategory(ProductCategory productCategory);
    /**
     * http://www.localhost:8989/business/productCategory/edit/80
     * 回显修改
     */
    ProductCategory editParentCategory(Long id);
    /**
     * http://www.localhost:8989/business/productCategory/update/80
     * 保存修改
     */
    void updateParentCategory(Long id, ProductCategory productCategory);

    /**
     * http://www.localhost:8989/business/productCategory/delete/80
     * 删除
     */
    void deleteParentCategory(Long id);
}
