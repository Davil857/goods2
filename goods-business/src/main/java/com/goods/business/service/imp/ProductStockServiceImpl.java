package com.goods.business.service.imp;

import com.goods.business.mapper.InformationMapper;
import com.goods.business.mapper.ProductStockMapper;
import com.goods.business.service.ProductStockService;
import com.goods.common.model.business.Product;
import com.goods.common.model.business.ProductStock;
import com.goods.common.vo.business.ProductStockVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@SuppressWarnings("all")
public class ProductStockServiceImpl implements ProductStockService {
    @Autowired
    private ProductStockMapper productStockMapper;
    @Autowired
    private InformationMapper informationMapper;
    /**
     * http://www.localhost:8989/business/product/findAllStocks?pageSize=9&pageNum=1
     * 扇形图：查找所有的库存
     */
    @Override
    public List<ProductStockVO> findAllStocks(Integer pageNum, Integer pageSize, Map map) {


        //类似new QueryWrapper
        Example example = new Example(Product.class);
        //创建查询条件
        Example.Criteria criteria = example.createCriteria();
        //判断传到后台的数据是否存在物资名称，模糊查询
        if(!"".equals(map.get("name"))&&null!=map.get("name")){
            criteria.andLike("name","%"+map.get("name")+"%");
        }
        if (!"".equals(map.get("categorys")) && null != map.get("categorys")) {
            //存在
            String categoryStr = (String) map.get("categorys");
            String[] split = categoryStr.split(",");
            criteria.andEqualTo("threeCategoryId", Long.valueOf(split[2]));
        }
        List<Product> productList=informationMapper.selectByExample(example);

        List<ProductStockVO> productStockVOSList = productList.stream().map(product -> {
            Example example1 = new Example(ProductStock.class);
            Example.Criteria criteria1 = example1.createCriteria();
            criteria1.andEqualTo("pNum",product.getPNum());
            List<ProductStock> productStocks = productStockMapper.selectByExample(example1);

            //ProductVO productVO = new ProductVO();
            ProductStockVO productStockVO = new ProductStockVO();
            productStockVO.setName(product.getName());
            productStockVO.setPNum(product.getPNum());
            productStockVO.setModel(product.getModel());
            productStockVO.setUnit(product.getUnit());
            productStockVO.setRemark(product.getRemark());
            if(!CollectionUtils.isEmpty(productStocks)){
                productStockVO.setStock(productStocks.get(0).getStock());
            }else{
                productStockVO.setStock(0L);
            }

            productStockVO.setImageUrl(product.getImageUrl());
            return productStockVO;
        }).collect(Collectors.toList());


        return productStockVOSList;


    }
}
