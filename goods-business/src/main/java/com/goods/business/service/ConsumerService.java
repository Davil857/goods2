package com.goods.business.service;

import com.goods.common.model.business.Consumer;
import com.goods.common.vo.business.ConsumerVO;
import com.goods.common.vo.system.PageVO;

import java.util.List;
import java.util.Map;

public interface ConsumerService {

    /**
     * http://www.localhost:8989/business/consumer/findConsumerList?pageNum=1&pageSize=10&name=
     * 页面分页展示
     */
    PageVO<ConsumerVO> findConsumerList(Integer pageNum, Integer pageSize, Map map);
    /**
     * http://www.localhost:8989/business/consumer/add
     * 新增
     */
    void addConsumerList(Consumer consumer);
    /**
     * http://www.localhost:8989/business/consumer/edit/30
     * 回显修改
     */
    Consumer editConsumerList(Long id);
    /**
     * http://www.localhost:8989/business/consumer/update/30
     * 保存更新
     */
    void updateConsumerList(Long id, Consumer consumer);
    /**
     * http://www.localhost:8989/business/consumer/delete/32
     * 删除
     */
    void deleteConsumerList(Long id);
    /**
     * http://www.localhost:8989/business/consumer/findAll
     * 查询所有的consumer
     */
    List<ConsumerVO> findAllconsumer();
}
