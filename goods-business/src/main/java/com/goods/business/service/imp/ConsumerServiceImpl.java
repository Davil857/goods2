package com.goods.business.service.imp;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.goods.business.mapper.ConsumeMapper;
import com.goods.business.service.ConsumerService;
import com.goods.common.model.business.Consumer;
import com.goods.common.vo.business.ConsumerVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@SuppressWarnings("all")
public class ConsumerServiceImpl implements ConsumerService {

    @Autowired
    private ConsumeMapper consumeMapper;
    /**
     * http://www.localhost:8989/business/consumer/findAll
     * 查询所有的consumer
     */
    @Override
    public List<ConsumerVO> findAllconsumer() {
        List<Consumer> consumerList = consumeMapper.selectAll();
        List<ConsumerVO> consumerVOList = consumerList.stream().map(consumer -> {
            ConsumerVO consumerVO = new ConsumerVO();
            BeanUtils.copyProperties(consumer, consumerVO);
            return consumerVO;
        }).collect(Collectors.toList());
        return consumerVOList;
    }

    /**
     * http://www.localhost:8989/business/consumer/delete/32
     * 删除
     */
    @Override
    public void deleteConsumerList(Long id) {
        consumeMapper.deleteByPrimaryKey(id);
    }

    /**
     * http://www.localhost:8989/business/consumer/update/30
     * 保存更新
     */
    @Override
    public void updateConsumerList(Long id, Consumer consumer) {
        consumer.setModifiedTime(new Date());
        consumer.setId(id);
        consumeMapper.updateByPrimaryKey(consumer);
    }

    /**
     * http://www.localhost:8989/business/consumer/edit/30
     * 回显修改
     */
    @Override
    public Consumer editConsumerList(Long id) {
        return consumeMapper.selectByPrimaryKey(id);
    }

    /**
     * http://www.localhost:8989/business/consumer/add
     * 新增
     */
    @Override
    public void addConsumerList(Consumer consumer) {
        consumer.setCreateTime(new Date());
        consumer.setModifiedTime(new Date());
        consumeMapper.insert(consumer);
    }

    /**
     * http://www.localhost:8989/business/consumer/findConsumerList?pageNum=1&pageSize=10&name=
     * 页面分页展示
     */
    @Override
    public PageVO<ConsumerVO> findConsumerList(Integer pageNum, Integer pageSize, Map map) {
        PageHelper.startPage(pageNum,pageSize);
        Example example = new Example(Consumer.class);
        Example.Criteria criteria = example.createCriteria();
        //省市区县 模糊
        if(!"".equals(map.get("address"))&&null!=map.get("address")){
            criteria.andLike("address","%"+map.get("address")+"%");
        }
            //联系人 精确
        if(!"".equals(map.get("contact"))&&null!=map.get("contact")){
            criteria.andEqualTo("contact",map.get("contact"));
        }
           // 具体地点  精确
        if(!"".equals(map.get("name"))&&null!=map.get("name")){
            criteria.andEqualTo("name",map.get("name"));
        }
        List<Consumer> consumers = consumeMapper.selectByExample(example);
        List<ConsumerVO> consumerVOList = consumers.stream().map(consumer -> {
            ConsumerVO consumerVO = new ConsumerVO();
            BeanUtils.copyProperties(consumer, consumerVO);
            return consumerVO;
        }).collect(Collectors.toList());
        PageInfo<Consumer> consumerPageInfo = new PageInfo<>(consumers);
        return new PageVO<ConsumerVO>(consumerPageInfo.getTotal(),consumerVOList);
    }

}
