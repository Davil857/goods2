package com.goods.business.service;

import com.goods.common.vo.business.ProductStockVO;

import java.util.List;
import java.util.Map;

public interface ProductStockService {
    /**
     * http://www.localhost:8989/business/product/findAllStocks?pageSize=9&pageNum=1
     * 扇形图：查找所有的库存
     */
    List<ProductStockVO> findAllStocks(Integer pageNum, Integer pageSize, Map map);
}
