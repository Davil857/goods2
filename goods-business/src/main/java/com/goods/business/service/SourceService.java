package com.goods.business.service;

import com.goods.common.model.business.Supplier;
import com.goods.common.vo.business.SupplierVO;
import com.goods.common.vo.system.PageVO;

import java.util.List;

public interface SourceService {
    /**
     * 分页列表展示
     * http://www.localhost:8989/business/supplier/findSupplierList?pageNum=1&pageSize=10&name=
     */
    PageVO<SupplierVO> findSupplierList(Integer pageNum, Integer pageSize, Supplier supplier);

    /**
     * http://www.localhost:8989/business/supplier/add
     * 添加
     */
    void addSupplier(Supplier supplier);


    /**
     * http://www.localhost:8989/business/supplier/edit/26
     * 回显编辑
     */
    Supplier editSupplier(Long id);
    /**
     * http://www.localhost:8989/business/supplier/update/26
     * 保存修改
     */
    void updateSupplier(Long id, Supplier supplier);
    /**
     * http://www.localhost:8989/business/supplier/delete/27
     * 删除
     */
    void deleteSupplier(Long id);
    /**
     * http://www.localhost:8989/business/supplier/findAll
     * 获取所有货物来源
     */
    List<SupplierVO> findAll();
}
