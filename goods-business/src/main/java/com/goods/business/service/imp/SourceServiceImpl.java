package com.goods.business.service.imp;

import com.goods.business.mapper.SourceMapper;
import com.goods.business.service.SourceService;
import com.goods.common.model.business.Supplier;
import com.goods.common.utils.ListPageUtils;
import com.goods.common.vo.business.SupplierVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@SuppressWarnings("all")
public class SourceServiceImpl implements SourceService {
    @Autowired
    private SourceMapper sourceMapper;
    /**
     * http://www.localhost:8989/business/supplier/findAll
     * 获取所有货物来源
     */
    @Override
    public List<SupplierVO> findAll() {
        List<Supplier> suppliers = sourceMapper.selectAll();
        List<SupplierVO> supplierVOList = suppliers.stream().map(supplier -> {
            SupplierVO supplierVO = new SupplierVO();
            BeanUtils.copyProperties(supplier, supplierVO);
            return supplierVO;
        }).collect(Collectors.toList());
        return supplierVOList;
    }

    /**
     * http://www.localhost:8989/business/supplier/delete/27
     * 删除
     */
    @Override
    public void deleteSupplier(Long id) {
        sourceMapper.deleteByPrimaryKey(id);
    }

    /**
     * http://www.localhost:8989/business/supplier/update/26
     * 保存修改
     */
    @Override
    public void updateSupplier(Long id, Supplier supplier) {
        supplier.setId(id);
        supplier.setModifiedTime(new Date());
        sourceMapper.updateByPrimaryKey(supplier);
    }

    /**
     * http://www.localhost:8989/business/supplier/edit/26
     * 回显编辑
     */
    @Override
    public Supplier editSupplier(Long id) {
        Supplier supplier = sourceMapper.selectByPrimaryKey(id);
        return supplier;
    }

    /**
     * http://www.localhost:8989/business/supplier/add
     * 添加
     */
    @Override
    public void addSupplier(Supplier supplier) {
        supplier.setCreateTime(new Date());
        supplier.setModifiedTime(new Date());
        sourceMapper.insert(supplier);
    }

    /**
     * 分页列表展示
     * http://www.localhost:8989/business/supplier/findSupplierList?pageNum=1&pageSize=10&name=
     */
    @Override
    public PageVO<SupplierVO> findSupplierList(Integer pageNum, Integer pageSize, Supplier supplier) {

        List<Supplier> supplierList=sourceMapper.findSupplierList(supplier);
        List<SupplierVO> supplierVOList = supplierList.stream().map(supplier1 -> {
            SupplierVO supplierVO = new SupplierVO();
            BeanUtils.copyProperties(supplier1, supplierVO);
            return supplierVO;
        }).collect(Collectors.toList());

        List<SupplierVO> pageList = ListPageUtils.page(supplierVOList, pageSize, pageNum);
        return new PageVO<SupplierVO>(supplierVOList.size(),pageList);

    }


}
