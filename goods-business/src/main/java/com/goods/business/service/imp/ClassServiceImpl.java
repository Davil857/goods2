package com.goods.business.service.imp;

import com.goods.business.mapper.ClassMapper;
import com.goods.business.service.ClassService;
import com.goods.common.error.BusinessCodeEnum;
import com.goods.common.error.BusinessException;
import com.goods.common.model.business.ProductCategory;
import com.goods.common.utils.CategoryTreeBuilder;
import com.goods.common.utils.ListPageUtils;
import com.goods.common.vo.business.ProductCategoryTreeNodeVO;
import com.goods.common.vo.system.PageVO;
import lombok.SneakyThrows;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@SuppressWarnings("all")
public class ClassServiceImpl implements ClassService {
    @Autowired
    private ClassMapper classMapper;
    /**
     * http://www.localhost:8989/business/productCategory/delete/80
     * 删除  TODO:未完善
     */
    @SneakyThrows
    @Override
    public void deleteParentCategory(Long id) {
        Example example = new Example(ProductCategory.class);
        example.createCriteria().andEqualTo("pid",id);
        List<ProductCategory> productCategoryList = classMapper.selectByExample(example);
        if(CollectionUtils.isEmpty(productCategoryList)){
            //throw new BusinessException(BusinessCodeEnum.PRODUCT_OUT_STOCK_EMPTY,"存在子分类");
            classMapper.deleteByPrimaryKey(id);
        }else{
            throw new BusinessException(BusinessCodeEnum.PRODUCT_OUT_STOCK_EMPTY,"存在子分类");
        }

    }

    /**
     * http://www.localhost:8989/business/productCategory/update/80
     * 保存修改
     */
    @Override
    public void updateParentCategory(Long id, ProductCategory productCategory) {
        productCategory.setId(id);
        productCategory.setModifiedTime(new Date());
        classMapper.updateByPrimaryKey(productCategory);
    }

    /**
     * http://www.localhost:8989/business/productCategory/edit/80
     * 回显修改
     */
    @Override
    public ProductCategory editParentCategory(Long id) {
        ProductCategory productCategory = classMapper.selectByPrimaryKey(id);
        return productCategory;
    }

    /**
     * 添加
     * http://www.localhost:8989/business/productCategory/add
     */
    @Override
    public void addParentCategory(ProductCategory productCategory) {
        productCategory.setModifiedTime(new Date());
        productCategory.setCreateTime(new Date());
        classMapper.insert(productCategory);
    }

    /**
     * 加载父类数据用于添加
     * http://www.localhost:8989/business/productCategory/getParentCategoryTree
     */
    @Override
    public List<ProductCategoryTreeNodeVO> getParentCategoryTree() {
        List<ProductCategory> productCategoryList = classMapper.selectAll();
        List<ProductCategoryTreeNodeVO> productCategoryTreeNodeVOS = productCategoryList.stream().map(productCategory -> {
            ProductCategoryTreeNodeVO productCategoryTreeNodeVO = new ProductCategoryTreeNodeVO();
            BeanUtils.copyProperties(productCategory, productCategoryTreeNodeVO);
            return productCategoryTreeNodeVO;
        }).collect(Collectors.toList());
        //构建多级
        List<ProductCategoryTreeNodeVO> buildList = CategoryTreeBuilder.build(productCategoryTreeNodeVOS);
        return buildList;
    }

    /**
     * 加载分类数据
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public PageVO<ProductCategoryTreeNodeVO> getcategoryTree(Integer pageNum, Integer pageSize) {
        List<ProductCategory> productCategoryList = classMapper.selectAll();
        List<ProductCategoryTreeNodeVO> productCategoryTreeNodeVOS = productCategoryList.stream().map(productCategory -> {
            ProductCategoryTreeNodeVO productCategoryTreeNodeVO = new ProductCategoryTreeNodeVO();
            BeanUtils.copyProperties(productCategory, productCategoryTreeNodeVO);
            return productCategoryTreeNodeVO;
        }).collect(Collectors.toList());
        //构建多级
        List<ProductCategoryTreeNodeVO> treeNodeVOList = CategoryTreeBuilder.build(productCategoryTreeNodeVOS);
        //分页
        List<ProductCategoryTreeNodeVO> pageList = ListPageUtils.page(treeNodeVOList, pageSize, pageNum);
        return new PageVO<>(treeNodeVOList.size(),pageList);


    }

}
