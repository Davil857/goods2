package com.goods.business.service.imp;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.goods.business.mapper.*;
import com.goods.business.service.InstockService;
import com.goods.common.model.business.*;
import com.goods.common.vo.business.InStockDetailVO;
import com.goods.common.vo.business.InStockItemVO;
import com.goods.common.vo.business.InStockVO;
import com.goods.common.vo.business.SupplierVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

@Service
@SuppressWarnings("all")
public class InstockServiceImpl implements InstockService {
    @Autowired
    private InstockMapper instockMapper;
    @Autowired
    private SourceMapper sourceMapper;
    @Autowired
    private InStockInfoMapper inStockInfoMapper;
    @Autowired
    private InformationMapper informationMapper;
    @Autowired
    private ProductStockMapper productStockMapper;
    /**
     * http://www.localhost:8989/business/inStock/publish/131
     * 审核通过
     */
    @Override
    public void publishIntoStock(Long id) {
        InStock inStock = instockMapper.selectByPrimaryKey(id);
        inStock.setStatus(0);
        instockMapper.updateByPrimaryKey(inStock);
    }

    /**
     * http://www.localhost:8989/business/inStock/addIntoStock
     * 新增
     */
    @Override
    @Transactional
    public void addIntoStock(InStockVO inStockVO) {
        if(inStockVO.getSupplierId()==null){
            //新增来源
            Supplier supplier = new Supplier();
            supplier.setName(inStockVO.getName());
            supplier.setAddress(inStockVO.getAddress());
            supplier.setEmail(inStockVO.getEmail());
            supplier.setPhone(inStockVO.getPhone());
            supplier.setModifiedTime(new Date());
            supplier.setCreateTime(new Date());
            supplier.setSort(inStockVO.getSort());
            supplier.setContact(inStockVO.getContact());
            sourceMapper.insert(supplier);
            inStockVO.setSupplierId(supplier.getId());
        }
        InStock inStock = new InStock();
        inStock.setInNum(UUID.randomUUID().toString().replace("-", ""));
        inStock.setType(inStockVO.getType());
        inStock.setOperator("admin");
        inStock.setSupplierId(inStockVO.getSupplierId());
        inStock.setCreateTime(new Date());
        inStock.setModified(new Date());
//        Object products = map.get("products");
//        products.getClass().get(1)
        List<Object> products = inStockVO.getProducts();
        List<Map<String,Integer>> productListOk=new ArrayList<>();
        for (Object product : products) {
            String jsonString = JSON.toJSONString(product);
            HashMap<String,Integer> map = JSON.parseObject(jsonString, HashMap.class);
            productListOk.add(map);
        }
        Integer productNumber=0;
        for (Map<String, Integer> map : productListOk) {
            productNumber+=map.get("productNumber");
        }
        inStock.setProductNumber(productNumber);
        inStock.setRemark(inStockVO.getRemark());
        //待审核
        inStock.setStatus(2);
        instockMapper.insert(inStock);
        //3.
        InStockInfo inStockInfo = new InStockInfo();
        for (Map<String, Integer> map : productListOk) {
            inStockInfo.setInNum(inStock.getInNum());
            Product product = informationMapper.selectByPrimaryKey(map.get("productId"));
            inStockInfo.setPNum(product.getPNum());
            inStockInfo.setProductNumber(map.get("productNumber"));
            inStockInfo.setCreateTime(new Date());
            inStockInfo.setModifiedTime(new Date());
            inStockInfoMapper.insert(inStockInfo);
            //更新总库存表
            Example example = new Example(ProductStock.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("pNum",inStockInfo.getPNum());
            List<ProductStock> productStocks = productStockMapper.selectByExample(example);
            if(!CollectionUtils.isEmpty(productStocks)){
                productStocks.get(0).setStock(productStocks.get(0).getStock()+map.get("productNumber").longValue());
                productStockMapper.updateByPrimaryKey(productStocks.get(0));
            }
            ProductStock productStock = new ProductStock();
            productStock.setPNum(inStockInfo.getPNum());
            productStock.setStock(map.get("productNumber").longValue());
            productStockMapper.insert(productStock);
        }


    }

    /**
     * http://www.localhost:8989/business/inStock/delete/117
     * 删除回收站
     */
    @Override
    public void deleteInStockList(Long id) {
        instockMapper.deleteByPrimaryKey(id);
    }

    /**
     * http://www.localhost:8989/business/inStock/back/116
     * 还原回收站
     */
    @Override
    public void backInStockList(Long id) {
        InStock inStock = instockMapper.selectByPrimaryKey(id);
        inStock.setStatus(0);
        instockMapper.updateByPrimaryKey(inStock);
    }

    /**
     * http://www.localhost:8989/business/inStock/remove/116
     * 移入回收站
     */
    @Override
    public void removeInStockList(Long id) {
        InStock inStock = instockMapper.selectByPrimaryKey(id);
        inStock.setStatus(1);
        instockMapper.updateByPrimaryKey(inStock);
    }

    /**
     * http://www.localhost:8989/business/inStock/detail/1?pageNum=1
     * 明细
     */
    @Override
    public InStockDetailVO getdetail(Long id, Integer pageNum) {
        InStock inStock = instockMapper.selectByPrimaryKey(id);

        Example example = new Example(InStockInfo.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("inNum",inStock.getInNum());
        List<InStockInfo> inStockInfos = inStockInfoMapper.selectByExample(example);

        List<InStockItemVO> itemVOS=new ArrayList<>();
        for (InStockInfo inStockInfo : inStockInfos) {

            Example example1 = new Example(Product.class);
            Example.Criteria criteria1 = example1.createCriteria();
            criteria1.andEqualTo("pNum",inStockInfo.getPNum());
            List<Product> products = informationMapper.selectByExample(example1);
            InStockItemVO inStockItemVO = new InStockItemVO();
            inStockItemVO.setId(products.get(0).getId());
            inStockItemVO.setPNum(products.get(0).getPNum());
            inStockItemVO.setName(products.get(0).getName());
            inStockItemVO.setModel(products.get(0).getModel());
            inStockItemVO.setUnit(products.get(0).getUnit());
            inStockItemVO.setImageUrl(products.get(0).getImageUrl());
            inStockItemVO.setCount(inStockInfo.getProductNumber());
            itemVOS.add(inStockItemVO);
        }
        InStockDetailVO inStockDetailVO = new InStockDetailVO();
        inStockDetailVO.setItemVOS(itemVOS);
        inStockDetailVO.setTotal(itemVOS.size());
        Supplier supplier = sourceMapper.selectByPrimaryKey(inStock.getSupplierId());
        SupplierVO supplierVO = new SupplierVO();
        BeanUtils.copyProperties(supplier,supplierVO);
        inStockDetailVO.setSupplierVO(supplierVO);
        inStockDetailVO.setOperator(inStock.getOperator());
        inStockDetailVO.setType(inStock.getType());
        inStockDetailVO.setStatus(inStock.getStatus());
        inStockDetailVO.setInNum(inStock.getInNum());
        return inStockDetailVO;
    }

    /**
     * http://www.localhost:8989/business/inStock/findInStockList?pageNum=1&pageSize=10&status=0
     * 页面分页展示
     */
    @Override
    public PageVO<InStockVO> findInStockList(Integer pageNum, Integer pageSize, InStockVO inStockVO) {
        PageHelper.startPage(pageNum,pageSize);
        List<InStockVO> inStockVOList =instockMapper.findInStockList(inStockVO);
//        List<InStockVO> inStockVoList = inStockList.stream().map(inStock -> {
//            InStockVO inStockVO1 = new InStockVO();
//            BeanUtils.copyProperties(inStock, inStockVO1);
//            return inStockVO1;
//        }).collect(Collectors.toList());

        PageInfo<InStock> inStockVOPageInfo = new PageInfo(inStockVOList);

        return new PageVO<InStockVO>(inStockVOPageInfo.getTotal(),inStockVOList);
    }
}
