package com.goods.business.service.imp;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.goods.business.mapper.InformationMapper;
import com.goods.business.mapper.ProductStockMapper;
import com.goods.business.service.InformationService;
import com.goods.common.model.business.Product;
import com.goods.common.model.business.ProductStock;
import com.goods.common.vo.business.ProductStockVO;
import com.goods.common.vo.business.ProductVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@SuppressWarnings("all")
public class InformationServiceImpl implements InformationService {
    @Autowired
    private InformationMapper informationMapper;
    @Autowired
    private ProductStockMapper productStockMapper;

    /**
     * http://www.localhost:8989/business/product/findProductStocks?pageSize=9&pageNum=1&categorys=81,98,99
     * 库存图右侧分页查询
     */
    @Override
    public PageVO<ProductStockVO> findProductStocks(Integer pageNum, Integer pageSize, Map map) {
        PageHelper.startPage(pageNum,pageSize);
        //类似new QueryWrapper
        Example example = new Example(Product.class);
        //创建查询条件
        Example.Criteria criteria = example.createCriteria();
        //判断传到后台的数据是否存在物资名称，模糊查询
        if(!"".equals(map.get("name"))&&null!=map.get("name")){
            criteria.andLike("name","%"+map.get("name")+"%");
        }
        if (!"".equals(map.get("categorys")) && null != map.get("categorys")) {
            //存在
            String categoryStr = (String) map.get("categorys");
            String[] split = categoryStr.split(",");
            criteria.andEqualTo("threeCategoryId", Long.valueOf(split[2]));
        }
        List<Product> productList=informationMapper.selectByExample(example);

        List<ProductStockVO> productStockVOSList = productList.stream().map(product -> {
            Example example1 = new Example(ProductStock.class);
            Example.Criteria criteria1 = example1.createCriteria();
            criteria1.andEqualTo("pNum",product.getPNum());
            List<ProductStock> productStocks = productStockMapper.selectByExample(example1);

            //ProductVO productVO = new ProductVO();
            ProductStockVO productStockVO = new ProductStockVO();
            productStockVO.setId(product.getId());
            productStockVO.setName(product.getName());
            productStockVO.setPNum(product.getPNum());
            productStockVO.setModel(product.getModel());
            productStockVO.setUnit(product.getUnit());
            productStockVO.setRemark(product.getRemark());
            if(!CollectionUtils.isEmpty(productStocks)){
                productStockVO.setStock(productStocks.get(0).getStock());
            }else{
                productStockVO.setStock(0L);
            }

            productStockVO.setImageUrl(product.getImageUrl());
            return productStockVO;
        }).collect(Collectors.toList());

        PageInfo<Product> productPageInfo = new PageInfo<>(productList);//用未封装之前的集合
        return new PageVO<ProductStockVO>(productPageInfo.getTotal(),productStockVOSList);
    }

    /**
     * http://www.localhost:8989/business/product/publish/62
     * 通过审核
     */
    @Override
    public void publishProductList(Long id) {
        Product product = informationMapper.selectByPrimaryKey(id);
        product.setStatus(0);
        informationMapper.updateByPrimaryKey(product);
    }

    /**
     * http://www.localhost:8989/business/product/delete/68
     * 回收站删除
     */
    @Override
    public void deleteProductList(Long id) {
        informationMapper.deleteByPrimaryKey(id);
    }

    /**
     * http://www.localhost:8989/business/product/back/66
     * 回收站恢复
     */
    @Override
    public void backProductList(Long id) {
        Product product = informationMapper.selectByPrimaryKey(id);
        product.setStatus(0);
        informationMapper.updateByPrimaryKey(product);
    }

    /**
     * http://www.localhost:8989/business/product/remove/66
     * 移到回收站
     */
    @Override
    public void removeProductList(Long id) {
        Product product = informationMapper.selectByPrimaryKey(id);
        product.setStatus(1);
        informationMapper.updateByPrimaryKey(product);
    }

    /**
     * http://www.localhost:8989/business/product/update/61
     * 保存修改
     */
    @Override
    public void updateProductList(Long id, Product product) {
        product.setId(id);
        product.setModifiedTime(new Date());
        List<Long> categoryKeys = product.getCategoryKeys();
        product.setOneCategoryId(categoryKeys.get(0));
        product.setTwoCategoryId(categoryKeys.get(1));
        product.setThreeCategoryId(categoryKeys.get(2));
        informationMapper.updateByPrimaryKey(product);
    }

    /**
     * http://www.localhost:8989/business/product/edit/61
     * 回显修改
     */
    @Override
    public Product editProductList(Long id) {
        return informationMapper.selectByPrimaryKey(id);
    }

    /**
     * http://www.localhost:8989/business/product/add
     * 添加
     */
    @Override
    public void addProductList(Product product) {

        product.setPNum(UUID.randomUUID().toString());
        List<Long> categoryKeys = product.getCategoryKeys();
        product.setOneCategoryId(categoryKeys.get(0));
        product.setTwoCategoryId(categoryKeys.get(1));
        product.setThreeCategoryId(categoryKeys.get(2));
        product.setModifiedTime(new Date());
        product.setCreateTime(new Date());
        product.setStatus(2);
        informationMapper.insert(product);

    }

    /**
     * http://www.localhost:8989/business/product/findProductList?pageNum=1&pageSize=6&name=&categoryId=&supplier=&status=0
     * 分页列表展示
     */
    @Override
    public PageVO<ProductVO> findProductList(Integer pageNum, Integer pageSize, Map map) {
        PageHelper.startPage(pageNum,pageSize);
        //类似new QueryWrapper
        Example example = new Example(Product.class);
        //创建查询条件
        Example.Criteria criteria = example.createCriteria();
        //判断传到后台的数据是否存在物资名称，模糊查询
        if(!"".equals(map.get("name"))&&null!=map.get("name")){
            criteria.andLike("name","%"+map.get("name")+"%");
        }
        //判断存到后台的是否存在物资状态  精确查询
        if(!"".equals(map.get("status"))&&null!=map.get("status")){
            criteria.andEqualTo("status",map.get("status"));
        }
        //判断是否存在分类信息  三级地址   精确查询
        /*if(!"".equals(map.get("categorys"))&&null!=map.get("categorys")){
            String categorysStr=(String)map.get("categorys");
            String[] split = categorysStr.split(",");
            criteria.andEqualTo("threeCategoryId",Long.valueOf(split[2]));
        }*/
        if (!"".equals(map.get("categorys")) && null != map.get("categorys")) {
            //存在
            String categoryStr = (String) map.get("categorys");
            String[] split = categoryStr.split(",");
            criteria.andEqualTo("threeCategoryId", Long.valueOf(split[2]));
        }
        List<Product> productList=informationMapper.selectByExample(example);
        List<ProductVO> productVOList = productList.stream().map(product -> {
            ProductVO productVO = new ProductVO();
            BeanUtils.copyProperties(product, productVO);
            return productVO;
        }).collect(Collectors.toList());

        //第一种分页：自定义工具类
        /*List<ProductVO> pageList = ListPageUtils.page(productVOList, pageSize, pageNum);
        return new PageVO<>(productVOList.size(),pageList);*/

        //第二种分页：用github的PageHelper
        /**
         * 方法开头PageHelper.startPage(pageNum,pageSize);//开启分页
         * 最后如下：用未封装之前的集合创建PageInfo，最后创建PageVO，然后
         * 将PageInfo的大小，和封装后的vo集合放进去
         */
        PageInfo<Product> productPageInfo = new PageInfo<>(productList);//用未封装之前的集合
        return new PageVO<ProductVO>(productPageInfo.getTotal(),productVOList);
    }
}
