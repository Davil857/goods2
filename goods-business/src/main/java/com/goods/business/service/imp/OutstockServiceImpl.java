package com.goods.business.service.imp;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.goods.business.mapper.*;
import com.goods.business.service.OutstockService;
import com.goods.common.error.BusinessCodeEnum;
import com.goods.common.error.BusinessException;
import com.goods.common.model.business.*;
import com.goods.common.vo.business.ConsumerVO;
import com.goods.common.vo.business.OutStockDetailVO;
import com.goods.common.vo.business.OutStockItemVO;
import com.goods.common.vo.business.OutStockVO;
import com.goods.common.vo.system.PageVO;
import lombok.SneakyThrows;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.*;
import java.util.stream.Collectors;

@Service
@SuppressWarnings("all")
public class OutstockServiceImpl implements OutstockService {
    @Autowired
    private OutstockMapper outstockMapper;
    @Autowired
    private ConsumeMapper consumeMapper;
    @Autowired
    private InformationMapper informationMapper;

    @Autowired
    private OutstockinfoMapper outstockinfoMapper;
    @Autowired
    private ProductStockMapper productStockMapper;
    /**
     * http://www.localhost:8989/business/outStock/publish/24
     * 通过审核
     */
    @Override
    public void publishOutStock(long id) {
        OutStock outStock = outstockMapper.selectByPrimaryKey(id);
        outStock.setStatus(0);
        outstockMapper.updateByPrimaryKey(outStock);
    }

    /**
     * http://www.localhost:8989/business/outStock/delete/2
     * 删除回收站
     */
    @Override
    public void deleteOutStock(long id) {
        outstockMapper.deleteByPrimaryKey(id);
    }

    /**
     * 恢复回收站
     * http://www.localhost:8989/business/outStock/back/15
     */
    @Override
    public void backOutStock(long id) {
        OutStock outStock = outstockMapper.selectByPrimaryKey(id);
        outStock.setStatus(0);
        outstockMapper.updateByPrimaryKey(outStock);
    }

    /**
     * 移到回收站
     * http://www.localhost:8989/business/outStock/remove/15
     */
    @Override
    public void removeOutStock(long id) {
        OutStock outStock = outstockMapper.selectByPrimaryKey(id);
        outStock.setStatus(1);
        outstockMapper.updateByPrimaryKey(outStock);
    }

    /**
     * http://www.localhost:8989/business/outStock/addOutStock
     * 添加
     */
    @Override
    @Transactional
    @SneakyThrows
    public void addOutStock(OutStockVO outStockVO) {
        if(outStockVO.getConsumerId()==null){
            //创建consumer
            Consumer consumer = new Consumer();

            consumer.setName(outStockVO.getName());
            consumer.setAddress(outStockVO.getAddress());
            consumer.setCreateTime(new Date());
            consumer.setModifiedTime(new Date());
            consumer.setPhone(outStockVO.getPhone());
            consumer.setSort(outStockVO.getSort());
            consumer.setContact(outStockVO.getContact());
            consumeMapper.insert(consumer);
            outStockVO.setConsumerId(consumer.getId());
        }
        OutStock outStock = new OutStock();

        outStock.setOutNum(UUID.randomUUID().toString().replace("-", ""));
        outStock.setType(outStockVO.getType());
        outStock.setOperator("admin");
        outStock.setCreateTime(new Date());

        List<Object> products = outStockVO.getProducts();
        List<Map<String,Integer>> productListOk=new ArrayList<>();
        for (Object product : products) {
            String jsonString = JSON.toJSONString(product);
            HashMap<String,Integer> map = JSON.parseObject(jsonString, HashMap.class);
            productListOk.add(map);
        }
        Integer productNumber=0;
        for (Map<String, Integer> map : productListOk) {
            productNumber+=map.get("productNumber");
        }


        outStock.setProductNumber(productNumber);//总件数量 TODO:productId是空的测试前记得处理

        outStock.setConsumerId(outStockVO.getConsumerId());
        outStock.setRemark(outStockVO.getRemark());
        outStock.setStatus(2);
        outStock.setPriority(outStockVO.getPriority());
        outstockMapper.insert(outStock);

        for (Map<String, Integer> map : productListOk) {
            OutStockInfo outStockInfo = new OutStockInfo();

            outStockInfo.setOutNum(outStock.getOutNum());
//            Example example = new Example(Product.class);
//            Example.Criteria criteria = example.createCriteria();
//            criteria.andEqualTo("")
            Product product = informationMapper.selectByPrimaryKey(map.get("productId"));

            outStockInfo.setPNum(product.getPNum());
            outStockInfo.setProductNumber(map.get("productNumber"));
            outStockInfo.setModifiedTime(new Date());
            outStockInfo.setCreateTime(new Date());
            outstockinfoMapper.insert(outStockInfo);
            //减去库存
            Example example = new Example(ProductStock.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("pNum",outStockInfo.getPNum());
            List<ProductStock> productStocks = productStockMapper.selectByExample(example);
            if(!CollectionUtils.isEmpty(productStocks)){
                if(productStocks.get(0).getStock()>map.get("productNumber")){
                    productStocks.get(0).setStock(productStocks.get(0).getStock()-map.get("productNumber"));
                    productStockMapper.updateByPrimaryKey(productStocks.get(0));
                }else{
                    throw new BusinessException(BusinessCodeEnum.PRODUCT_STOCK_ERROR,"库存不足");
                }
            }
        }

        //TODO:productId是空的测试前记得处理
    }

    /**
     * http://www.localhost:8989/business/outStock/detail/15?pageNum=1
     * 明细 GET
     */
    @Override
    @Transactional
    public OutStockDetailVO detailOutStockList(Long id, Integer pageNum) {
        OutStockDetailVO outStockDetailVO = new OutStockDetailVO();
        OutStock outStock = outstockMapper.selectByPrimaryKey(id);

        outStockDetailVO.setOutNum(outStock.getOutNum());
        outStockDetailVO.setStatus(outStock.getStatus());
        outStockDetailVO.setType(outStock.getType());
        outStockDetailVO.setOperator("admin");
        Consumer consumer = consumeMapper.selectByPrimaryKey(outStock.getConsumerId());
        ConsumerVO consumerVO = new ConsumerVO();
        BeanUtils.copyProperties(consumer,consumerVO);
        outStockDetailVO.setConsumerVO(consumerVO);

        List<OutStockItemVO> itemVOS=new ArrayList<>();
        Long totals=0L;
        Example example = new Example(OutStockInfo.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("outNum",outStock.getOutNum());
        List<OutStockInfo> outStockInfos = outstockinfoMapper.selectByExample(example);
        for (OutStockInfo outStockInfo : outStockInfos) {
//            outStockInfo.getPNum();
//            outStockInfo.getProductNumber();
            Example example1 = new Example(Product.class);
            Example.Criteria criteria1 = example1.createCriteria();
            criteria1.andEqualTo("pNum",outStockInfo.getPNum());

            List<Product> products = informationMapper.selectByExample(example1);

            if(!CollectionUtils.isEmpty(products)){
                OutStockItemVO outStockItemVO = new OutStockItemVO();

                outStockItemVO.setPNum(products.get(0).getPNum());
                outStockItemVO.setName(products.get(0).getName());
                outStockItemVO.setModel(products.get(0).getModel());
                outStockItemVO.setUnit(products.get(0).getUnit());
                outStockItemVO.setImageUrl(products.get(0).getImageUrl());
                outStockItemVO.setCount(outStockInfo.getProductNumber());
                itemVOS.add(outStockItemVO);
            }
            totals++;


        }
        outStockDetailVO.setItemVOS(itemVOS);
        outStockDetailVO.setTotal(totals);
        return outStockDetailVO;

    }

    /**
     * http://www.localhost:8989/business/outStock/findOutStockList?pageNum=1&pageSize=10&status=0
     * 分页展示
     */
    @Override
    public PageVO<OutStockVO> findOutStockList(Integer pageNum, Integer pageSize, Map map) {
        PageHelper.startPage(pageNum,pageSize);
        Example example = new Example(OutStock.class);
        Example.Criteria criteria = example.createCriteria();
        //发放单号
        if(!"".equals(map.get("outNum"))&&null!=map.get("outNum")){
            criteria.andEqualTo("outNum",map.get("outNum"));
        }
            //发放类型
        if(!"".equals(map.get("type"))&&null!=map.get("type")){
            criteria.andEqualTo("type",map.get("type"));
        }
            //发放状态
        if(!"".equals(map.get("status"))&&null!=map.get("status")){
            criteria.andEqualTo("status",map.get("status"));
        }
        List<OutStock> outStocks = outstockMapper.selectByExample(example);
        List<OutStockVO> outStockVOList = outStocks.stream().map(outStock -> {
            OutStockVO outStockVO = new OutStockVO();
            BeanUtils.copyProperties(outStock, outStockVO);
            Consumer consumer = consumeMapper.selectByPrimaryKey(outStock.getConsumerId());
            outStockVO.setName(consumer.getName());
            outStockVO.setPhone(consumer.getPhone());
            return outStockVO;
        }).collect(Collectors.toList());
        PageInfo<OutStock> outStockPageInfo = new PageInfo<>(outStocks);
        return new PageVO<OutStockVO>(outStockPageInfo.getTotal(),outStockVOList);
    }

}
