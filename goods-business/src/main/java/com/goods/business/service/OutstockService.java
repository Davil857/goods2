package com.goods.business.service;

import com.goods.common.vo.business.OutStockDetailVO;
import com.goods.common.vo.business.OutStockVO;
import com.goods.common.vo.system.PageVO;

import java.util.Map;

public interface OutstockService {
    /**
     * http://www.localhost:8989/business/outStock/findOutStockList?pageNum=1&pageSize=10&status=0
     * 分页展示
     */
    PageVO<OutStockVO> findOutStockList(Integer pageNum, Integer pageSize, Map map);
    /**
     * http://www.localhost:8989/business/outStock/detail/15?pageNum=1
     * 明细 GET
     */
    OutStockDetailVO detailOutStockList(Long id, Integer pageNum);
    /**
     * http://www.localhost:8989/business/outStock/addOutStock
     * 添加
     */
    void addOutStock(OutStockVO outStockVO);
    /**
     * 移到回收站
     * http://www.localhost:8989/business/outStock/remove/15
     */
    void removeOutStock(long id);
    /**
     * 恢复回收站
     * http://www.localhost:8989/business/outStock/back/15
     */
    void backOutStock(long id);
    /**
     * http://www.localhost:8989/business/outStock/delete/2
     * 删除回收站
     */
    void deleteOutStock(long id);
    /**
     * http://www.localhost:8989/business/outStock/publish/24
     * 通过审核
     */
    void publishOutStock(long id);
}
