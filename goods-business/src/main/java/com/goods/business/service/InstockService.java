package com.goods.business.service;

import com.goods.common.vo.business.InStockDetailVO;
import com.goods.common.vo.business.InStockVO;
import com.goods.common.vo.system.PageVO;

public interface InstockService {
    /**
     * http://www.localhost:8989/business/inStock/findInStockList?pageNum=1&pageSize=10&status=0
     * 页面分页展示
     */
    PageVO<InStockVO> findInStockList(Integer pageNum, Integer pageSize, InStockVO inStockVO);
    /**
     * http://www.localhost:8989/business/inStock/detail/1?pageNum=1
     * 明细
     */
    InStockDetailVO getdetail(Long id, Integer pageNum);
    /**
     * http://www.localhost:8989/business/inStock/remove/116
     * 移入回收站
     */
    void removeInStockList(Long id);
    /**
     * http://www.localhost:8989/business/inStock/back/116
     * 还原回收站
     */
    void backInStockList(Long id);
    /**
     * http://www.localhost:8989/business/inStock/delete/117
     * 删除回收站
     */
    void deleteInStockList(Long id);
    /**
     * http://www.localhost:8989/business/inStock/addIntoStock
     * 新增
     */
    void addIntoStock(InStockVO inStockVO);
    /**
     * http://www.localhost:8989/business/inStock/publish/131
     * 审核通过
     */
    void publishIntoStock(Long id);
}
