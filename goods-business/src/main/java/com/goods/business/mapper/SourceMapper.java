package com.goods.business.mapper;

import com.goods.common.model.business.Supplier;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface SourceMapper extends Mapper<Supplier> {
    /**
     * 分页列表展示
     * http://www.localhost:8989/business/supplier/findSupplierList?pageNum=1&pageSize=10&name=
     */
    List<Supplier> findSupplierList(Supplier supplier);
}
