package com.goods.business.mapper;

import com.goods.common.model.business.InStock;
import com.goods.common.vo.business.InStockVO;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface InstockMapper extends Mapper<InStock> {
    /**
     * http://www.localhost:8989/business/inStock/findInStockList?pageNum=1&pageSize=10&status=0
     * 页面分页展示
     */
    List<InStockVO> findInStockList(InStockVO inStockVO);
}
