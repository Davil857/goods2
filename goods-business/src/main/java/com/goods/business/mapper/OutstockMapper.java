package com.goods.business.mapper;


import com.goods.common.model.business.OutStock;
import tk.mybatis.mapper.common.Mapper;

public interface OutstockMapper extends Mapper<OutStock> {
}
