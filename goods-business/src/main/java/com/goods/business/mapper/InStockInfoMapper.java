package com.goods.business.mapper;

import com.goods.common.model.business.InStockInfo;
import tk.mybatis.mapper.common.Mapper;

public interface InStockInfoMapper extends Mapper<InStockInfo> {
}
