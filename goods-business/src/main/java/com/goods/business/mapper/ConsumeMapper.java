package com.goods.business.mapper;

import com.goods.common.model.business.Consumer;
import tk.mybatis.mapper.common.Mapper;

public interface ConsumeMapper extends Mapper<Consumer> {
}
