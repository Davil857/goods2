package com.goods.controller.business;

import com.goods.business.service.InformationService;
import com.goods.business.service.ProductStockService;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.business.ProductStockVO;
import com.goods.common.vo.system.PageVO;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@Api(tags="物资库存")
@RestController
@RequestMapping("/business/product")
public class PictstockController {
    @Autowired
    private InformationService informationService;
    @Autowired
    private ProductStockService productStockService;
//    /**
//     * http://www.localhost:8989/business/product/findProductStocks?pageSize=9&pageNum=1
//     */
//    @GetMapping("findProductStocks")
//    public
    /**
     * http://www.localhost:8989/business/product/findProductStocks?pageSize=9&pageNum=1&categorys=81,98,99
     * 库存图右侧分页查询
     */
    @GetMapping("findProductStocks")
    public ResponseBean<PageVO<ProductStockVO>> findProductStocks(@RequestParam Integer pageNum,
                                                                     @RequestParam Integer pageSize,
                                                                     @RequestParam Map map){
        PageVO<ProductStockVO> productStockVOPageVO=informationService.findProductStocks(pageNum,pageSize,map);
        return ResponseBean.success(productStockVOPageVO);
    }
    /**
     * http://www.localhost:8989/business/product/findAllStocks?pageSize=9&pageNum=1
     * 扇形图：查找所有的库存
     */
    @GetMapping("findAllStocks")
    public ResponseBean<List<ProductStockVO>> findAllStocks(@RequestParam Integer pageNum,
                                                          @RequestParam Integer pageSize,
                                                            @RequestParam Map map){
        List<ProductStockVO> productStockVOPageVO=productStockService.findAllStocks(pageNum,pageSize,map);
        return ResponseBean.success(productStockVOPageVO);
    }


}
