package com.goods.controller.business;

import com.goods.business.service.OutstockService;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.business.OutStockDetailVO;
import com.goods.common.vo.business.OutStockVO;
import com.goods.common.vo.system.PageVO;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Api(tags="发放记录")
@RestController
@RequestMapping("/business/outStock")
public class OutstockController {
    @Autowired
    private OutstockService outstockService;
    /**
     * http://www.localhost:8989/business/outStock/publish/24
     * 通过审核
     */
    @PutMapping("publish/{id}")
    public ResponseBean publishOutStock(@PathVariable long id){
        outstockService.publishOutStock(id);
        return ResponseBean.success();
    }
    /**
     * http://www.localhost:8989/business/outStock/delete/2
     * 删除回收站
     */
    @GetMapping("delete/{id}")
    public ResponseBean deleteOutStock(@PathVariable long id){
        outstockService.deleteOutStock(id);
        return ResponseBean.success();
    }
    /**
     * 恢复回收站
     * http://www.localhost:8989/business/outStock/back/15
     */
    @PutMapping("back/{id}")
    public ResponseBean backOutStock(@PathVariable long id){
        outstockService.backOutStock(id);
        return ResponseBean.success();
    }
    /**
     * 移到回收站
     * http://www.localhost:8989/business/outStock/remove/15
     */
    @PutMapping("remove/{id}")
    public ResponseBean removeOutStock(@PathVariable long id){
        outstockService.removeOutStock(id);
        return ResponseBean.success();
    }
    /**
     * http://www.localhost:8989/business/outStock/addOutStock
     * 添加
     */
    @PostMapping("addOutStock")
    public ResponseBean addOutStock(@RequestBody OutStockVO outStockVO){
        outstockService.addOutStock(outStockVO);
        return ResponseBean.success();
    }
    /**
     * http://www.localhost:8989/business/outStock/findOutStockList?pageNum=1&pageSize=10&status=0
     * 分页展示
     */
    @GetMapping("findOutStockList")
    public ResponseBean<PageVO<OutStockVO>> findOutStockList(@RequestParam Integer pageNum,
                                                             @RequestParam Integer pageSize,
                                                             @RequestParam Map map){
        PageVO<OutStockVO> OutStockList=outstockService.findOutStockList(pageNum,pageSize,map);
        return ResponseBean.success(OutStockList);
    }
    /**
     * http://www.localhost:8989/business/outStock/detail/15?pageNum=1
     * 明细 GET
     */
    @GetMapping("detail/{id}")
    public ResponseBean<OutStockDetailVO> detailOutStockList(@PathVariable Long id,@RequestParam Integer pageNum){
        OutStockDetailVO outStockDetailVO=outstockService.detailOutStockList(id,pageNum);
        return ResponseBean.success(outStockDetailVO);
    }
}
