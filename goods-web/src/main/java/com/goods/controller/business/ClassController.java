package com.goods.controller.business;

import com.goods.business.service.ClassService;
import com.goods.common.model.business.ProductCategory;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.business.ProductCategoryTreeNodeVO;
import com.goods.common.vo.system.PageVO;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags="物资类别")
@RestController
@RequestMapping("/business/productCategory")
public class ClassController {
    @Autowired
    private ClassService classService;

    /**
     * 加载分类数据
     * http://www.localhost:8989/business/productCategory/categoryTree
     * @param pageNum
     * @param pageSize
     * @return
     */
    @GetMapping("categoryTree")
    public ResponseBean<PageVO<ProductCategoryTreeNodeVO>> getcategoryTree(@RequestParam Integer pageNum, @RequestParam Integer pageSize){
            PageVO<ProductCategoryTreeNodeVO> productCategoryTreeNodeVOList= classService.getcategoryTree(pageNum,pageSize);
            //PageVO<ProductCategoryTreeNodeVO> categoryList = categroyService.getCategoryTree(pageNum,pageSize);
            return ResponseBean.success(productCategoryTreeNodeVOList);
    }
    /**
     * 加载父类数据用于添加
     * http://www.localhost:8989/business/productCategory/getParentCategoryTree
     */
    @GetMapping("getParentCategoryTree")
    public ResponseBean<List<ProductCategoryTreeNodeVO>> getParentCategoryTree(){
        List<ProductCategoryTreeNodeVO> productCategoryTreeNodeVOList=classService.getParentCategoryTree();
        return ResponseBean.success(productCategoryTreeNodeVOList);
    }
    /**
     * 添加
     * http://www.localhost:8989/business/productCategory/add
     */
    @PostMapping("add")
    public ResponseBean addParentCategory(@RequestBody ProductCategory productCategory){
        classService.addParentCategory(productCategory);
        return ResponseBean.success();
    }
    /**
     * http://www.localhost:8989/business/productCategory/edit/80
     * 回显修改
     */
    @GetMapping("edit/{id}")
    public ResponseBean<ProductCategory> editParentCategory(@PathVariable Long id){
        ProductCategory productCategory=classService.editParentCategory(id);
        return ResponseBean.success(productCategory);
    }
    /**
     * http://www.localhost:8989/business/productCategory/update/80
     * 保存修改
     */
    @PutMapping("update/{id}")
    public ResponseBean updateParentCategory(@PathVariable Long id,
                                                              @RequestBody ProductCategory productCategory){
        classService.updateParentCategory(id,productCategory);
        return ResponseBean.success();
    }
    /**
     * http://www.localhost:8989/business/productCategory/delete/80
     * 删除
     */
    @DeleteMapping("delete/{id}")
    public ResponseBean deleteParentCategory(@PathVariable Long id){
        classService.deleteParentCategory(id);
        return ResponseBean.success();
    }


}
