package com.goods.controller.business;

import com.goods.business.service.InformationService;
import com.goods.common.model.business.Product;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.business.ProductVO;
import com.goods.common.vo.system.PageVO;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Api(tags="物资资料")
@RestController
@RequestMapping("/business/product")
public class InformationController {
    @Autowired
    private InformationService informationService;
    /**
     * http://www.localhost:8989/business/product/findProducts?pageNum=1&pageSize=6&status=0&categorys=81,98,99
     * 查询分类列表用于添加入库记录
     */
    @GetMapping("findProducts")
    public ResponseBean<PageVO<ProductVO>> findProducts(@RequestParam Integer pageNum,
                                                           @RequestParam Integer pageSize,
                                                        @RequestParam Map map){
        PageVO<ProductVO> productVOList=informationService.findProductList(pageNum,pageSize,map);
        return ResponseBean.success(productVOList);
    }
    /**
     * http://www.localhost:8989/business/product/findProductList?pageNum=1&pageSize=6&name=&categoryId=&supplier=&status=0
     * 分页列表展示
     */
    @GetMapping("findProductList")
    public ResponseBean<PageVO<ProductVO>> findProductList(@RequestParam Integer pageNum,
                                                           @RequestParam Integer pageSize,
                                                           @RequestParam Map map){
        PageVO<ProductVO> productVOList=informationService.findProductList(pageNum,pageSize,map);
        return ResponseBean.success(productVOList);
    }
    /**
     * http://www.localhost:8989/business/product/add
     * 添加
     */
    @PostMapping("add")
    public ResponseBean addProductList(@RequestBody Product product){
        informationService.addProductList(product);
        return ResponseBean.success();
    }
    /**
     * http://www.localhost:8989/business/product/edit/61
     * 回显修改
     */
    @GetMapping("edit/{id}")
    public ResponseBean<Product> editProductList(@PathVariable Long id){
        Product product=informationService.editProductList(id);
        return ResponseBean.success(product);
    }
    /**
     * http://www.localhost:8989/business/product/update/61
     * 保存修改
     */
    @PutMapping("update/{id}")
    public ResponseBean updateProductList(@PathVariable Long id,@RequestBody Product product){
        informationService.updateProductList(id,product);
        return ResponseBean.success();
    }
    /**
     * http://www.localhost:8989/business/product/remove/66
     * 移到回收站
     */
    @PutMapping("remove/{id}")
    public ResponseBean removeProductList(@PathVariable Long id){
        informationService.removeProductList(id);
        return ResponseBean.success();
    }
    /**
     * http://www.localhost:8989/business/product/back/66
     * 回收站恢复
     */
    @PutMapping("back/{id}")
    public ResponseBean backProductList(@PathVariable Long id){
        informationService.backProductList(id);
        return ResponseBean.success();
    }
    /**
     * http://www.localhost:8989/business/product/delete/68
     * 回收站删除
     */
    @DeleteMapping("delete/{id}")
    public ResponseBean deleteProductList(@PathVariable Long id){
        informationService.deleteProductList(id);
        return ResponseBean.success();
    }
    /**
     * http://www.localhost:8989/business/product/publish/62
     * 通过审核
     */
    @PutMapping("publish/{id}")
    public ResponseBean publishProductList(@PathVariable Long id){
        informationService.publishProductList(id);
        return ResponseBean.success();
    }
}
