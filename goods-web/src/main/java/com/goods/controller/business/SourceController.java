package com.goods.controller.business;

import com.goods.business.service.SourceService;
import com.goods.common.model.business.Supplier;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.business.SupplierVO;
import com.goods.common.vo.system.PageVO;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags="物资来源")
@RestController
@RequestMapping("/business/supplier")
public class SourceController {
    @Autowired
    private SourceService sourceService;
    /**
     * http://www.localhost:8989/business/supplier/findAll
     * 获取所有货物来源
     */
    @GetMapping("findAll")
    public ResponseBean<List<SupplierVO>> findAll(){
        List<SupplierVO> supplierVOList=sourceService.findAll();
        return ResponseBean.success(supplierVOList);
    }
    /**
     * 分页列表展示
     * http://www.localhost:8989/business/supplier/findSupplierList?pageNum=1&pageSize=10&name=
     */
    @GetMapping("findSupplierList")
    public ResponseBean<PageVO<SupplierVO>> findSupplierList(@RequestParam Integer pageNum,
                                                             @RequestParam Integer pageSize,
                                                             Supplier supplier){
        PageVO<SupplierVO> supplierListPage=sourceService.findSupplierList(pageNum,pageSize,supplier);
        return ResponseBean.success(supplierListPage);
    }
    /**
     * http://www.localhost:8989/business/supplier/add
     * 添加
     */
    @PostMapping("add")
    public ResponseBean addSupplier(@RequestBody Supplier supplier){
        sourceService.addSupplier(supplier);
        return ResponseBean.success();
    }
    /**
     * http://www.localhost:8989/business/supplier/edit/26
     * 回显编辑
     */
    @GetMapping("edit/{id}")
    public ResponseBean<Supplier> editSupplier(@PathVariable Long id){
        Supplier supplier=sourceService.editSupplier(id);
        return ResponseBean.success(supplier);
    }
    /**
     * http://www.localhost:8989/business/supplier/update/26
     * 保存修改
     */
    @PutMapping("update/{id}")
    public ResponseBean updateSupplier(@PathVariable Long id,
                                       @RequestBody Supplier supplier){
        sourceService.updateSupplier(id,supplier);
        return ResponseBean.success();
    }
    /**
     * http://www.localhost:8989/business/supplier/delete/27
     * 删除
     */
    @DeleteMapping("delete/{id}")
    public ResponseBean deleteSupplier(@PathVariable Long id){
        sourceService.deleteSupplier(id);
        return ResponseBean.success();
    }
}
