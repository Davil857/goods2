package com.goods.controller.business;

import com.goods.business.service.ConsumerService;
import com.goods.common.model.business.Consumer;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.business.ConsumerVO;
import com.goods.common.vo.system.PageVO;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Api(tags="物资去处")
@RestController
@RequestMapping("/business/consumer")
public class ConsumerController {

    @Autowired
    private ConsumerService consumerService;


    /**
     * http://www.localhost:8989/business/consumer/findAll
     * 查询所有的consumer
     */
    @GetMapping("findAll")
    public ResponseBean<List<ConsumerVO>> findAllconsumer(){
        List<ConsumerVO> consumerList=consumerService.findAllconsumer();
        return ResponseBean.success(consumerList);

    }
    /**
     * http://www.localhost:8989/business/consumer/findConsumerList?pageNum=1&pageSize=10&name=
     * 页面分页展示
     */
    @GetMapping("findConsumerList")
    public ResponseBean<PageVO<ConsumerVO>> findConsumerList(@RequestParam Integer pageNum,
                                                             @RequestParam Integer pageSize,
                                                             @RequestParam Map map){
        PageVO<ConsumerVO> outStockVOPageVO=consumerService.findConsumerList(pageNum,pageSize,map);
        return ResponseBean.success(outStockVOPageVO);
    }
    /**
     * http://www.localhost:8989/business/consumer/add
     * 新增
     */
    @PostMapping("add")
    public ResponseBean addConsumerList(@RequestBody Consumer consumer){
        consumerService.addConsumerList(consumer);
        return ResponseBean.success();
    }
    /**
     * http://www.localhost:8989/business/consumer/edit/30
     * 回显修改
     */
    @GetMapping("edit/{id}")
    public ResponseBean<Consumer> editConsumerList(@PathVariable Long id){
        Consumer consumer=consumerService.editConsumerList(id);
        return ResponseBean.success(consumer);
    }
    /**
     * http://www.localhost:8989/business/consumer/update/30
     * 保存更新
     */
    @PutMapping("update/{id}")
    public ResponseBean updateConsumerList(@PathVariable Long id,@RequestBody Consumer consumer){
        consumerService.updateConsumerList(id,consumer);
        return ResponseBean.success();
    }
    /**
     * http://www.localhost:8989/business/consumer/delete/32
     * 删除
     */
    @DeleteMapping("delete/{id}")
    public ResponseBean deleteConsumerList(@PathVariable Long id){
        consumerService.deleteConsumerList(id);
        return ResponseBean.success();
    }
}
