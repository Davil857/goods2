package com.goods.controller.business;

import com.goods.business.service.InstockService;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.business.InStockDetailVO;
import com.goods.common.vo.business.InStockVO;
import com.goods.common.vo.system.PageVO;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags="物资入库")
@RestController
@RequestMapping("/business/inStock")
public class InstockController {

    @Autowired
    private InstockService instockService;
    /**
     * http://www.localhost:8989/business/inStock/publish/131
     * 审核通过
     */
    @PutMapping("publish/{id}")
    public ResponseBean publishIntoStock(@PathVariable Long id){
        instockService.publishIntoStock(id);
        return ResponseBean.success();
    }
    /**
     * http://www.localhost:8989/business/inStock/addIntoStock
     * 新增
     */
    @PostMapping("addIntoStock")
    public ResponseBean addIntoStock(@RequestBody InStockVO inStockVO){
        instockService.addIntoStock(inStockVO);
        return ResponseBean.success();
    }
    /**
     * http://www.localhost:8989/business/inStock/findInStockList?pageNum=1&pageSize=10&status=0
     * 页面分页展示
     */
    @GetMapping("findInStockList")
    public ResponseBean<PageVO<InStockVO>> findInStockList(@RequestParam Integer pageNum,
                                                           @RequestParam Integer pageSize,
                                                           InStockVO inStockVO){
        PageVO<InStockVO> pageVOList=instockService.findInStockList(pageNum,pageSize,inStockVO);
        return ResponseBean.success(pageVOList);
    }
    /**
     *
     * http://www.localhost:8989/business/inStock/detail/112?pageNum=1
     * 明细
     */
    @GetMapping("/detail/{id}")
    public ResponseBean<InStockDetailVO> getdetail(@PathVariable Long id, @RequestParam Integer pageNum){
        InStockDetailVO getdetail = instockService.getdetail(id, pageNum);
        return ResponseBean.success(getdetail);
    }
    /**
     * http://www.localhost:8989/business/inStock/remove/116
     * 移入回收站
     */
    @PutMapping("remove/{id}")
    public ResponseBean removeInStockList(@PathVariable Long id){
        instockService.removeInStockList(id);
        return ResponseBean.success();
    }
    /**
     * http://www.localhost:8989/business/inStock/back/116
     * 还原回收站
     */
    @PutMapping("back/{id}")
    public ResponseBean backInStockList(@PathVariable Long id){
        instockService.backInStockList(id);
        return ResponseBean.success();
    }
    /**
     * http://www.localhost:8989/business/inStock/delete/117
     * 删除回收站
     */
    @GetMapping("delete/{id}")
    public ResponseBean deleteInStockList(@PathVariable Long id){
        instockService.deleteInStockList(id);
        return ResponseBean.success();
    }
}
